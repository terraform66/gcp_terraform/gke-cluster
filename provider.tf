
provider "google" {
    credentials = file("./credentials.json")
    project = "using-terraf-156-7b427441"
    region = var.region-common
    version     = "~> 3.5.0"
}
